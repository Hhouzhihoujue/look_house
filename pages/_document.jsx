import Document, { Head, Main, NextScript } from 'next/document'


export default class MyDocument extends Document {
  // static async getInitialProps(ctx) {
  //   const initialProps = await Document.getInitialProps(ctx)
  //   return { ...initialProps }
  // }

  render() {
    return (
      <html>
        <Head>
          <link rel="shortcut icon" href="/static/favicon.ico" />
          <link rel="stylesheet" href="/_next/static/css/styles.chunk.css" />
          <title>dodo</title>
          <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;" name="viewport" />
        </Head>
        <body className="custom_class">
          <Main />
          <NextScript />
          <script src="http://cache.amap.com/lbs/static/es5.min.js"></script>
          <script src="https://webapi.amap.com/maps?v=1.4.10&amp;key=8cde1c9cb3f9c71167b6d0cddf9bac81&amp;plugin=AMap.MarkerClusterer"></script>
          <script src="https://webapi.amap.com/ui/1.0/main.js?v=1.0.11"></script>
        </body>
      </html>
    )
  }
}