module.exports = {
  apps: [
    {
      name: 'look-house',
      script: './server',
      instances: 'max',
      exec_mode: 'cluster',
      max_memory_restart: '500M',
      env: {
        NODE_ENV: 'production'
      }
    }
  ]
}
