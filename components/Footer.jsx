import React from 'react'

export default class Footer extends React.Component {
  render() {
    return (
      <footer className="main-footer">
        <div className="do-common-container">
          <span className="main-footer-item">&copy;2018 dodo</span>
        </div>
      </footer>
    );
  }
}
