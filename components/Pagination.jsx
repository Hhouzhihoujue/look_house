import React from 'react';
import classnames from 'classnames'


export default class Pagination extends React.Component {
  render() {
    const { current, total = 0, pageSize = 10, onChange } = this.props
    const maxPage = parseInt(total / pageSize)

    return (
      <div className="ze-pagination">
        {
          Array.from({ length: maxPage }).map((item, index) => (
            <button
              key={index}
              onClick={() => onChange(index + 1)}
              className={classnames('ze-pagination-item', current === index + 1 && 'active')}
            >
              {index + 1}
            </button>
          ))
        }
      </div>
    )
  }
}
