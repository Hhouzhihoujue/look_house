import React from 'react';
import Link from 'next/link'


export default class Header extends React.Component {
  render() {
    return (
      <header className="main-header">
        <div className="do-common-container main-header-container">
          <Link href="/index">
            <a className="logo-brand">
              <img src="/static/logo.svg" alt="" />
              <div className="logo-text">
                <span className="logo-text-title">查房网</span>
              </div>
            </a>
          </Link>

          <div className="main-header-links">
            <Link href="/index">
              <a>主页</a>
            </Link>
            <Link href="/price">
              <a>价格</a>
            </Link>
          </div>
        </div>
      </header>
    )
  }
}
